import asyncio

import pytest
from aiohttp import web

from conf.setting import config


@pytest.fixture(scope="session")
def loop(request):
    loop = asyncio.get_event_loop_policy().new_event_loop()
    yield loop
    loop.close()


@pytest.fixture(scope="function")
async def mock_http_200(loop, aiohttp_server):
    async def handler(request):
        return web.HTTPOk()

    app = web.Application()
    app.router.add_get("/", handler)
    server = await aiohttp_server(app, port=config["HTTP_SRV_PORT"])
    yield server


@pytest.fixture(scope="function")
async def mock_http_400(loop, aiohttp_server):
    async def handler(request):
        return web.HTTPBadRequest()

    app = web.Application()
    app.router.add_get("/", handler)
    server = await aiohttp_server(app, port=config["HTTP_SRV_PORT"])
    yield server
