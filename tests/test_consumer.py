import asyncio

from applications.consume.consumer import Worker
from conf.setting import config


async def test_retry_http_request_success(mock_http_200, loop):
    worker = Worker(loop=loop, worker_num=1, channel=None)
    res = await worker._retry_http_request(
        config["HTTP_SRV_URI"], config["CONSUME_WAITING"]
    )
    assert res is True


async def mock_awaitable():
    await asyncio.sleep(0)


async def test_retry_http_request_bad(mock_http_400, loop):
    worker = Worker(loop=loop, worker_num=1, channel=None)
    res = await worker._retry_http_request(
        config["HTTP_SRV_URI"], config["CONSUME_WAITING"]
    )
    assert res is False
