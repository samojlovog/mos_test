import asyncio
import logging

import aio_pika

from applications.produce.publisher import Publisher
from conf.setting import config

logger = logging.getLogger(__name__)


async def main(loop: asyncio.AbstractEventLoop):
    connection = await aio_pika.connect_robust(config["RABBIT_DSN"], loop=loop)

    channel = await connection.channel()
    exchange = await channel.declare_exchange(
        "test_exchange",
        type=aio_pika.ExchangeType.DIRECT,
        durable=True,
        auto_delete=False,
    )
    queue = await channel.declare_queue(
        name=config["PRODUCE_QUEUE_NAME"], durable=True, auto_delete=False
    )
    await queue.bind(exchange=exchange)

    producers = []
    for i in range(config["PRODUCE_COUNT"]):
        producers.append(Publisher(loop=loop, publisher_num=i, exchange=exchange))
    await asyncio.wait([i.start(config["PRODUCE_QUEUE_NAME"]) for i in producers])

    await connection.close()


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main(loop=loop))
    loop.close()
