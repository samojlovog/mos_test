from applications.utils import env


def updater(conf: dict, part: tuple):
    conf.update({part[0]: part[1]})


config: dict = {}

# RabbitMQ
updater(config, env("RABBIT_DSN", str))

# Test http server
updater(config, env("HTTP_SRV_HOST", str))
updater(config, env("HTTP_SRV_PORT", int))
updater(config, env("HTTP_SRV_URI", str))

# Consuming
updater(config, env("CONSUME_WORKERS_COUNT", int))
updater(config, env("CONSUME_QUEUE_NAME", str))
updater(config, env("CONSUME_WAITING", int))

# Producing
updater(config, env("PRODUCE_COUNT", int))
updater(config, env("PRODUCE_QUEUE_NAME", str))
