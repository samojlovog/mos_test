import asyncio
import logging

import aio_pika

from applications.consume.consumer import Worker
from conf.setting import config

logger = logging.getLogger(__name__)


async def main(loop: asyncio.AbstractEventLoop):
    connection = await aio_pika.connect_robust(config["RABBIT_DSN"], loop=loop)

    channel = await connection.channel()
    await channel.declare_queue(
        name=config["CONSUME_QUEUE_NAME"], durable=True, auto_delete=False
    )

    workers = []
    for i in range(config["CONSUME_WORKERS_COUNT"]):
        workers.append(Worker(loop=loop, worker_num=i, channel=channel))
    await asyncio.wait(
        [
            i.start(
                config["CONSUME_QUEUE_NAME"],
                config["HTTP_SRV_URI"],
                config["CONSUME_WAITING"],
            )
            for i in workers
        ]
    )

    await connection.close()


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main(loop=loop))
    loop.close()
