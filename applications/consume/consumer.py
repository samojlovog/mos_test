import asyncio
import logging

import aio_pika.pool
import aiohttp

logger = logging.getLogger(__name__)


class Worker:
    """Class for starting consuming process."""

    def __init__(
        self,
        loop: asyncio.AbstractEventLoop,
        worker_num: int,
        channel: aio_pika.Channel,
    ):
        self._loop = loop
        self._worker_num = worker_num
        self._channel = channel

    async def start(self, queue_name: str, http_uri: str, wait_sec: int):
        logger.info(f"worker #{self._worker_num}: starting ...")
        async with self._channel as channel:
            await channel.set_qos(1)
            queue = await channel.declare_queue(
                queue_name, durable=True, auto_delete=False
            )
            async with queue.iterator() as queue_iter:
                async for message in queue_iter:
                    async with message.process():

                        logger.info(
                            f"worker #{self._worker_num}: consumed. Msg: {message.body.decode()}"
                        )
                        await self._retry_http_request(http_uri, wait_sec)

    async def _retry_http_request(self, url: str, wait_sec: int) -> bool:
        async with aiohttp.ClientSession() as session:
            for i in range(5):
                async with session.get(url) as response:
                    if response.status == 200:
                        logger.info(f"worker #{self._worker_num}: success request")
                        return True
                    logger.info(f"worker #{self._worker_num}: bad request")
                    await asyncio.sleep(wait_sec)
            else:
                logger.info(f"worker #{self._worker_num}: five request failed!")
                return False
