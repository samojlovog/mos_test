import asyncio
import logging
import uuid

import aio_pika

logger = logging.getLogger(__name__)


class Publisher:
    """Class for starting producing process."""

    def __init__(
        self,
        loop: asyncio.AbstractEventLoop,
        publisher_num: int,
        exchange: aio_pika.Exchange,
    ):
        self._loop = loop
        self._publisher_num = publisher_num
        self._exchange = exchange

    async def start(self, queue_name: str):
        logger.info(f"producer #{self._publisher_num}: starting ...")
        while True:
            message = aio_pika.Message(
                body=f"{uuid.uuid4()}".encode(),
                delivery_mode=aio_pika.DeliveryMode.PERSISTENT,
            )
            await self._exchange.publish(message=message, routing_key=queue_name)

            logger.info(
                f"producer #{self._publisher_num}: published {message.body.decode()}"
            )
            await asyncio.sleep(1)
