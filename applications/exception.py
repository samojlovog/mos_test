class EnvValueException(Exception):
    pass


class EnvValueNotFoundException(EnvValueException):
    pass


class EnvValueIncorrectException(EnvValueException):
    pass
