import json
import logging
import os

from applications.const import UNDEFINED
from applications.exception import EnvValueIncorrectException, EnvValueNotFoundException

logging.basicConfig(format="%(name)s - %(levelname)s - %(message)s", level=logging.INFO)


def _parse_bool(raw_str: str) -> bool:
    if raw_str.lower() == "true":
        return True
    elif raw_str.lower() == "false":
        return False
    else:
        raise EnvValueIncorrectException()


def env(
    env_key: str, typecast, default=UNDEFINED, comment=None, sensitive=False
) -> tuple:
    try:
        raw_value = os.environ[env_key]
    except KeyError:
        if default is not UNDEFINED:
            raw_value = default
        else:
            raise EnvValueNotFoundException()
    if typecast is bool:
        value = _parse_bool(raw_value)
    elif typecast is list:
        value = json.loads(raw_value)
    elif typecast is tuple:
        value = tuple(json.loads(raw_value))
    elif typecast is dict:
        value = json.loads(raw_value)
    elif typecast in [str, int]:
        value = typecast(raw_value)
    else:
        raise ValueError(
            f"{env_key}: typecast is not a primitive. Got {typecast} instead"
        )

    if not isinstance(value, typecast):
        raise ValueError(
            f"Can't treat '{raw_value}' as {typecast}. Look up to {env_key} environment variable"
        )

    return env_key, value
