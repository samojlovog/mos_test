from random import randint, seed

from aiohttp import web

from conf.setting import config

seed(100)


async def handle(request):
    num = randint(0, 36)
    if int(num) % 4 == 0:
        return web.HTTPOk()
    return web.HTTPBadRequest()


app = web.Application()
app.add_routes([web.get("/", handle)])

web.run_app(app, host=config["HTTP_SRV_HOST"], port=config["HTTP_SRV_PORT"])
