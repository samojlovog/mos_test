Mos.ru test project
================== 

Prerequisites
-------------
- Python 3.6
- pipenv
- Docker, docker-compose (v3)


Deploy
------

```bash
cd /project/dir

# setup requirement ENV param
nano .env.prod

cp .env.prod .env

# create virtualenv with dependencies
pipenv install --dev

# start RabbitMQ
docker-compose up -d

# start http server
pipenv run python run_http_server.py

# start producing process
pipenv run python run_produce.py

# start consuming process
pipenv run python run_produce.py
```

Testing
-------

```bash
cp .env.test .env
pipenv run pytest
```

Author
------
Oleg Samoilov <samoilovog@yandex.ru>